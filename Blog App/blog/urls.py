from django.urls import path
from .views import PostList, PostDetailView
from .feeds import LatestPostFeed

urlpatterns = [
    path('posts/', PostList.as_view(), name='post-list'),
    path('post/<int:pk>', PostDetailView.as_view(), name='post-detail'),
    path('feed/', LatestPostFeed(), name='post-feed'),

]
