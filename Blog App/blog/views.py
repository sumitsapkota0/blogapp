from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.views.generic import ListView, View, TemplateView, DetailView
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Post, Comment
from .forms import CommentForm

# using LISTVIEW
# class PostList(ListView):
#     template_name = 'blog/post_list.html'
#     queryset = Post.objects.all()
#     context_object_name = 'posts'

# using TemplateView
# class PostList(TemplateView):
#     template_name = 'blog/post_list.html'
#
#     def get_context_data(self, *args, **kwargs):
#         context = super(PostList, self).get_context_data(*args, **kwargs)
#         context['posts'] = Post.objects.all()
#         return context

# using View


class PostList(View):

    def get(self, request, *args, **kwargs):
        # to filter the data from table
        # posts = get_list_or_404(Post.objects.filter(status='published'), *args, **kwargs)
        posts = Post.objects.all()
        paginator = Paginator(posts, 5)
        page = request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        return render(request, 'blog/post_list.html', {'posts': posts, 'page': page})


class PostDetailView(View):

    # context_object_name = 'posts'

    # handling of comment forms

    def get(self, request, *args, **kwargs):
        post = get_object_or_404(Post, pk=kwargs['pk'])
        top_comments = post.comments.filter(
            active=True).order_by('-created')[:10]
        all_comments = post.comments.filter(active=True)
        form = CommentForm(request.GET or None)
        return render(request, 'blog/post_details.html', {'posts': post, 'form': form, 'comments': top_comments, 'all_comments': all_comments})

    def post(self, request, *args, **kwargs):
        posts = Post.objects.get(pk=self.kwargs['pk'])
        comment_form = CommentForm(data=request.POST)
        post = get_object_or_404(Post, pk=kwargs['pk'])

        comments = post.comments.filter(active=True)
        all_comments = post.comments.filter(active=True)

        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = posts
            new_comment.save()
            messages.success(request, 'Comment successfully added.')

        else:
            raise forms.ValidationError("Enter comment again")

        return render(request, 'blog/post_details.html', {'posts': posts, 'form': comment_form, 'comments': comments, 'all_comments': all_comments})
